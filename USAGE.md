# Synopsis
`genpl [OPTIONS] TIME`

Où `TIME` est un paramètre obligatoire et est la durée de la playlist exprimée en minutes (en interne, l'ensemble du temps est géré à la seconde).

# Les `OPTIONS` disponibles
## Globales
*  `--name TEXT` alphanumérique, `playlist` par défaut
*  `--path PATH` relatif ou absolu, `.` par défaut
*  `--loglevel LEVEL` où le niveau de criticité `LEVEL` d'affichage sur la sortie des erreurs peut être : `DEBUG`, `INFO`, `NOTICE`, `WARNING`, `ERROR` (par défaut), `CRITICAL`, `ALERT`, `EMERGENCY`
*  `--M3U` par défaut si non défini
*  `--XSPF`
*  `--description TEXT` pour permettre de fournir des informations à stocker dans les exports XSPF, inutile dans le cas d'un `M3U`

## De sélection selon critères
`TEXT` correspond à la valeur de recherche et `QTY` le pourcentage de la quantité escomptée
*  `--genre TEXT=QTY`
*  `--subgenre TEXT=QTY`
*  `--artist TEXT=QTY`
*  `--album TEXT=QTY`
*  `--title TEXT=QTY`

## De configuration de la connexion à la base de données
Par défaut le programme va chercher la configuration de la connexion à la base de données dans les fichiers suivants au format JSON (le premier trouvé est prioritaire) : `~/.config/genpl/config.json`, `~/.genpl.json`, `/etc/genpl/config.json`.

Si les options suivantes sont définies, elles deviennent prioritaires sur les valeurs trouvées dans le fichier de configuration :
*  `--config FILENAME` où `FILENAME` est le fichier avec le chemin relatif ou absolu contenant la configuration
*  `--db-user TEXT`
*  `--db-database TEXT`
*  `--db-schema TEXT`
*  `--db-host TEXT`
*  `--db-port-tcp TEXT`
