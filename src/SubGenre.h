/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_SUBGENRE_H_
#define GENPL_SRC_SUBGENRE_H_

#include <string>
#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class SubGenre
 *
 * \brief SubGenre corresponding to the data model \c sousgenre
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql#L43
 */
class SubGenre
{
 private:
  uint16_t id;  ///< Object identifier
  std::string name;  ///< SubGenre name string

 public:
  /** \brief Default constructor
   *
   * With null #id and #name
   */
  explicit SubGenre();

  /** \brief Detailed constructor
   *
   * Construct a #SubGenre object based on the detailed parameterization
   *
   * \param[in] _id The #SubGenre identification number
   *
   * \param[in] _name The #SubGenre name string
   */
  explicit SubGenre(uint16_t _id,
                    std::string _name);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #SubGenre object from the first query result row given
   * in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, name FROM sousgenre WHERE id=23;");
   * SubGenre my_new_subgenre(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _subgenre_result A pointer to the result of a PostgreSQL query
   */
  explicit SubGenre(PGresult *_subgenre_result);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the object's #name
   *
   * \return A copy of the #name \c std::string
   */
  std::string getName();

  /** \brief Set the object's #name
   *
   * \param[in] _name The \c std::string to use as #name
   */
  void setName(std::string _name);
};
}

#endif  // GENPL_SRC_SUBGENRE_H_
