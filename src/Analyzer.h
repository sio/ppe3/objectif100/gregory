/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_ANALYZER_H_
#define GENPL_SRC_ANALYZER_H_

#include <cstdint>
#include <string>
#include <chrono>
#include <map>

#include <log4cxx/logger.h>
#include <tclap/CmdLine.h>

#include "Criteria.h"
#include "Configuration.h"

namespace TCLAP {
template<>
struct ArgTraits<cli::Criterion>
{
  typedef StringLike ValueCategory;
};
}

namespace cli {

/** \class Analyzer
 *
 * \brief Command Line Interface (CLI) analyzer
 *
 * This class is an helper to the CLI management. It contains options
 * and parameters waited on CLI, directly or through other classes
 * like Criteria or Configuration.
 *
 * \note
 * Implements singleton design pattern
 */
class Analyzer
{
 private:
  static Analyzer *singleton;  ///< Pointer to the static singleton class member
  int argc;  ///< Keep track of the \c argc value passed when parsing
  char **argv;  ///< Keep track of the \c argv value passed when parsing

  // Global options
  uint16_t duration;  ///< Requested playlist duration in minutes
  std::string name;  ///< Requested playlist name
  std::string path;  ///< Requested playlist output directory path
  std::string description;  ///< Requested playlist description (not used in M3U output)
  std::string loglevel;  ///< Runtime log level
  bool M3U_output;  ///< Requested playlist output to M3U format
  bool XSPF_output;  ///< Requested playlist output to XSPF format
  // Criteria options
  cli::Criteria criteria;  ///< Requested playlist criteria collection (title, genre, etc.)
  // Database options
  std::string db_config_file;  ///< Requested database config file
  db::Configuration db_configuration;  ///< Requested database configuration elements

  /** \brief Default constructor
   *
   * This constructor is not pubicly accessible. Use the instance()
   * member function instead to access the singleton
   */
  Analyzer();

 public:
  /** \brief Obtain singleton as pointer
   */
  static Analyzer *instance();

  /** \brief Analyze CLI and value accordingly
   */
  void parse(int _argc, char **_argv);

  /** \brief Print out summary
   */
  void summary() const;

  /** \brief Get the pointer to Criteria
   *
   * \return The constant pointer to Criteria stored in #criteria
   */
  const cli::Criteria *getCriteria();

  /** \brief Get the pointer to Configuration
   *
   * \return The constant pointer to Configuration stored in #configuration
   */
  const db::Configuration *getConfiguration();

  /** \brief Get the database configuration filename
   *
   * \return The path and filename string
   */
  std::string getDB_config_file();

  /** \brief Get the user defined playlist #duration
   *
   * \return The #duration in minutes
   */
  uint16_t getDuration();

  /** \brief Get the user defined playlist name
   *
   * \return The name
   */
  std::string getName();

  /** \brief Get the user defined playlist saving path
   *
   * \return The path
   */
  std::string getPath();

  /** \brief Get the user defined log level
   *
   * \return The log level name string
   */
  std::string getLogLevel();

  /** \brief Get the user defined M3U export flag
   *
   * \return The M3U export flag
   */
  bool getM3U_output();

  /** \brief Get the user defined XSPF export flag
   *
   * \return The XSPF export flag
   */
  bool getXSPF_output();

  /** \brief Get the user defined XSPF description text
   *
   * \return The XSPF description text
   */
  std::string getDescription();

  /** \brief Get the user defined \c argc counter
   *
   * \return The argument counter value
   */
  int getArgc();

  /** \brief Get the user defined \c argv array
   *
   * \return The \c argv array
   */
  char **getArgv();
};
}

#endif
