/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_POLYPHONY_H_
#define GENPL_SRC_POLYPHONY_H_

#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class Polyphony
 *
 * \brief Polyphony corresponding to the data model \c polyphonie
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql#L39
 */
class Polyphony
{
 private:
  uint16_t id;  ///< Object identifier
  uint8_t number;  ///< Polyphony channel number

 public:
  /** \brief Default constructor
   *
   * With null #id and #number
   */
  explicit Polyphony();

  /** \brief Detailed constructor
   *
   * Construct a #Polyphony object based on the detailed
   * parameterization
   *
   * \param[in] _id The #Polyphony identification number
   *
   * \param[in] _number The #Polyphony channel number
   */
  explicit Polyphony(uint16_t _id,
                     uint8_t _number);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #Polyphony object from the first query result row
   * given in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, number FROM sousgenre WHERE id=23;");
   * Polyphony my_new_polyphony(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _polyphony_result A pointer to the result of a PostgreSQL query
   */
  explicit Polyphony(PGresult *_polyphony_result);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the object's #number
   *
   * \return A copy of the #number
   */
  uint8_t getNumber();

  /** \brief Set the object's #number
   *
   * \param[in] _number The channel #number to set
   */
  void setNumber(uint8_t _number);
};
}

#endif  // GENPL_SRC_POLYPHONY_H_
