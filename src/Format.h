/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_FORMAT_H_
#define GENPL_SRC_FORMAT_H_

#include <string>
#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class Format
 *
 * \brief Format corresponding to the data model \c format
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql
 */
class Format
{
 private:
  uint16_t id;  ///< Object identifier
  std::string description;  ///< Format description string

 public:
  /** \brief Default constructor
   *
   * With null #id and #description
   */
  explicit Format();

  /** \brief Detailed constructor
   *
   * Construct a #Format object based on the detailed parameterization
   *
   * \param[in] _id The identification number
   *
   * \param[in] _description The #Format's description string
   */
  explicit Format(uint16_t _id,
                  std::string _description);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #Format object from the first query result row given
   * in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, description FROM Format WHERE id=23;");
   * Format my_new_format(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _format_result A pointer to the result of a PostgreSQL query
   */
  explicit Format(PGresult *_format_result);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the object's #description
   *
   * \return A copy of the #description \c std::string
   */
  std::string getDescription();

  /** \brief Set the object's #description
   *
   * \param[in] _description The \c std::string to use as #description
   */
  void setDescription(std::string _description);
};
}

#endif  // GENPL_SRC_FORMAT_H_
