/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_ALBUM_H_
#define GENPL_SRC_ALBUM_H_

#include <chrono>
#include <string>
#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class Album
 *
 * \brief Album corresponding to the data model \c album
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql#L4
 */
class Album
{
 private:
  uint16_t id;  ///< Object identifier
  std::chrono::system_clock date;  ///< Album release date
  std::string name;  ///< Artist name string

 public:
  /** \brief Default constructor
   *
   * With blank #id, #date and #name
   */
  explicit Album();

  /** \brief Detailed constructor
   *
   * Construct an #Album object based on the detailed parameterization
   *
   * \param[in] _id The identification number
   *
   * \param[in] _date The release date
   *
   * \param[in] _name The #Album's name string
   */
  explicit Album(uint16_t _id,
                 std::chrono::system_clock _date,
                 std::string _name);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #Album object from the first query result row given
   * in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, nom FROM album WHERE id=23;");
   * Album my_new_album(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _album_result A pointer to the result of a PostgreSQL query
   */
  explicit Album(PGresult *_album_result);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the Album release date
   *
   * \return The Album release date
   */
  std::chrono::system_clock getDate();

  /** \brief Set the Album's release date
   *
   * \param[in] _date The release date
   */
  void setDate(std::chrono::system_clock _date);

  /** \brief Get the name of the Album
   *
   * \return The Album name
   */
  std::string getName();

  /** \brief Set the name of the Album
   *
   * \param[in] _name The name to set
   */
  void setName(std::string _name);
};
}

#endif  // GENPL_SRC_ALBUM_H_
