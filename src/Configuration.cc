/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Configuration.h"

#include <iostream>
#include <fstream>
#include <stdexcept>

#include <json/json.h>

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE".db"));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("db"));
#endif

db::Configuration::Configuration() :
  user("default_user"),
  database("default_database"),
  schema("default_schema"),
  host("default_host"),
  portTCP(5432)
{}


db::Configuration::Configuration(std::string _user,
                                 std::string _database,
                                 std::string _schema,
                                 std::string _host,
                                 uint16_t _portTCP) :
  user(_user),
  database(_database),
  schema(_schema),
  host(_host),
  portTCP(_portTCP)
{}

void db::Configuration::load(std::string _config_file)
{
  Json::Value json;
  std::ifstream input_file;
  std::string errors;
  input_file.open(_config_file);
  Json::CharReaderBuilder builder;
  builder["collectComments"] = false;

  if(Json::parseFromStream(builder, input_file, &json, &errors))
  {
    user = json["db"].get("user", user).asString();
    database = json["db"].get("database", database).asString();
    schema = json["db"].get("schema", schema).asString();
    host = json["db"].get("host", host).asString();
    portTCP = json["db"].get("portTCP", portTCP).asUInt();
  }
  else
  {
    logger->error("Database Json configuration file error when parsing " + _config_file);
  }

  input_file.close();
}

void db::Configuration::summary() const
{
  std::ostringstream output;
  output << "[Database configuration summary]" << std::endl;
  output << "  User: " << user << std::endl
         << "  Database: " << database << std::endl
         << "  Schema: " << schema << std::endl
         << "  FQDN host: " << host << std::endl
         << "  TCP port: " << portTCP << std::endl;
  logger->info(output.str());
}

std::string db::Configuration::getUser()
{
  return user;
}

void db::Configuration::setUser(std::string _user)
{
  user = _user;
}

std::string db::Configuration::getDatabase()
{
  return database;
}

void db::Configuration::setDatabase(std::string _database)
{
  database = _database;
}

std::string db::Configuration::getSchema()
{
  return schema;
}

void db::Configuration::setSchema(std::string _schema)
{
  schema = _schema;
}

std::string db::Configuration::getHost()
{
  return host;
}

void db::Configuration::setHost(std::string _host)
{
  host = _host;
}

uint16_t db::Configuration::getPortTCP()
{
  return portTCP;
}

void db::Configuration::setPortTCP(uint16_t _portTCP)
{
  portTCP = _portTCP;
}
