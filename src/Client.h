/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_CLIENT_H_
#define GENPL_SRC_CLIENT_H_

#include <string>

#include <libpq-fe.h>

#include "Configuration.h"

namespace db {

/** \class Client
 *
 * \brief Helper class to manage PostgreSQL connection
 *
 * The aim of this class is to encapsulate access, connection and data
 * manipulation with a PostgreSQL server. It uses the \c libpq inside
 * and provides a convenient way to interact directly, in C++, with
 * the library.
 */
class Client
{
 private:
  db::Configuration *configuration;   ///< Database connection configuration
  PGconn *connection;   ///< Connection handler

 public:
  /** \brief Default constructor
   *
   * With nullptr #configuration and #connection
   */
  explicit Client();

  /** \brief Constructor with #configuration parameter
   *
   * \param[in] _configuration Pointer to the Configuration object
   */
  explicit Client(db::Configuration *_configuration);

  /** \brief Get the Configuration pointer
   *
   * \return A constant pointer to the #configuration
   */
  const db::Configuration *getConfiguration();

  /** \brief Set the address to which the member #configuration should point to
   *
   * \param[in,out] _configuration Pointer to the new Configuration object
   */
  void setConfiguration(db::Configuration *_configuration);

  /** \brief Establish connection
   *
   * Establish connection with the server, based on #configuration
   * parameters, using the \c libpq \c PQconnectdbParams
   */
  void connect();

  /** \brief Break connection
   *
   * If the connection is established and the #connection handler is
   * correct, disconnect it using the \c libpq \c PQfinish.
   */
  void disconnect();

  /** \brief Get the #connection handler
   *
   * \return A constant pointer to the #connection handler
   */
  const PGconn *getConnection();

  /** \brief Wrapper to the PQexec function
   *
   * \param _request SQL request to be sent and executed on the remote
   * server
   *
   * \return The server result as a \c PGresult pointer
   */
  PGresult *execute(std::string _request);
};
}

#endif  // GENPL_SRC_CLIENT_H_
