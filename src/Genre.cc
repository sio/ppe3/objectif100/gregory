/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Genre.h"

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE".audio"));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("audio"));
#endif
