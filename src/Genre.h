/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_GENRE_H_
#define GENPL_SRC_GENRE_H_

#include <string>
#include <cstdint>

#include <libpq-fe.h>

namespace audio {

/** \class Genre
 *
 * \brief Genre corresponding to the data model \c genre
 *
 * \sa The data model in the PostgreSQL database
 * https://framagit.org/sio/ppe3/generateur-de-playlist/blob/master/RadioLibre.sql#L25
 */
class Genre
{
 private:
  uint16_t id;  ///< Object identifier
  std::string name;  ///< Genre name string

 public:
  /** \brief Default constructor
   *
   * With null #id and #name
   */
  explicit Genre();

  /** \brief Detailed constructor
   *
   * Construct a #Genre object based on the detailed parameterization
   *
   * \param[in] _id The #Genre identification number
   *
   * \param[in] _name The #Genre name string
   */
  explicit Genre(uint16_t _id,
                 std::string _name);

  /** \brief Constructor from a PostgreSQL query result
   *
   * Construct a #Genre object from the first query result row given
   * in parameter.
   *
   * \warning The pointer is not freed after construction. This is the
   * caller responsibility.
   * \code{.cpp}
   * PGconn * conn = ...;
   * PGresult * result = PQexec(conn, "SELECT id, name FROM Genre WHERE id=23;");
   * Genre my_new_genre(result);
   * PQclear(result);
   * \endcode
   *
   * \param[in] _genre_result A pointer to the result of a PostgreSQL query
   */
  explicit Genre(PGresult *_genre_result);

  /** \brief Get the object's identifier
   *
   * \return A copy of the #id number
   */
  uint16_t getId();

  /** \brief Set the object's #id entification number
   *
   * \param[in] _id The number to use as identifier
   */
  void setId(uint16_t _id);

  /** \brief Get the object's #name
   *
   * \return A copy of the #name \c std::string
   */
  std::string getName();

  /** \brief Set the object's #name
   *
   * \param[in] _name The \c std::string to use as #name
   */
  void setName(std::string _name);
};
}

#endif  // GENPL_SRC_GENRE_H_
