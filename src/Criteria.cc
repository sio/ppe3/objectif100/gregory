/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
 */

#include "Criteria.h"

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE".cli"));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("cli"));
#endif

cli::Criterion::Criterion() :
  pattern(""),
  quantity(0)
{}

cli::Criterion::Criterion(std::string _pattern, uint16_t _quantity) :
  pattern(_pattern),
  quantity(_quantity)
{}

cli::Criterion::~Criterion() {}

cli::Criterion &cli::Criterion::operator=(const std::string &input_string)
{
  auto const position = input_string.find_last_of('=');

  if(position != std::string::npos)
  {
    pattern = input_string.substr(0, position);

    try
    {
      int q_value = std::stoi(input_string.substr(position + 1).c_str());

      if(q_value > 0 and q_value <= 100)
      {
        quantity = q_value;
      }
      else
      {
        throw std::invalid_argument("");
      }
    }
    catch(std::invalid_argument &e)
    {
      throw TCLAP::ArgParseException("'" + input_string.substr(position + 1) + "' is not a valid QUANTITY for the pattern '" + pattern + "', please provide a numeric integer [1..100]");
    }
  }
  else
  {
    throw TCLAP::ArgParseException(input_string + " is not a valid Criterion form, please provide a PATTERN=QUANTITY form");
  }

  return *this;
}

std::string cli::Criterion::summary() const
{
  std::ostringstream output;
  output << quantity << "% of " << pattern;
  return output.str();
}


cli::Criteria::Criteria() {}

void cli::Criteria::summary() const
{
  std::ostringstream output;
  output << std::endl << "[Criteria summary]" << std::endl;

  if(titles.size() > 0)
  {
    output << "  Titles:" << std::endl;

    for(auto title : titles)
    {
      output << "    " << title.summary() << std::endl;
    }
  }

  if(genres.size() > 0)
  {
    output << "  Genres:" << std::endl;

    for(auto genre : genres)
    {
      output << "    " << genre.summary() << std::endl;
    }
  }

  if(subgenres.size() > 0)
  {
    output << "  SubGenres:" << std::endl;

    for(auto subgenre : subgenres)
    {
      output << "    " << subgenre.summary() << std::endl;
    }
  }

  if(artists.size() > 0)
  {
    output << "  Artists:" << std::endl;

    for(auto artist : artists)
    {
      output << "    " << artist.summary() << std::endl;
    }
  }

  if(albums.size() > 0)
  {
    output << "  Albums:" << std::endl;

    for(auto album : albums)
    {
      output << "    " << album.summary() << std::endl;
    }
  }

  logger->info(output.str());
}

std::vector<cli::Criterion> cli::Criteria::getTitles()
{
  return std::vector<cli::Criterion>();
}

void cli::Criteria::setTitles(std::vector<cli::Criterion> _new_titles)
{
  titles = _new_titles;
}

void cli::Criteria::addTitle(std::string _title, uint16_t _quantity)
{
  titles.push_back(cli::Criterion(_title, _quantity));
}

void cli::Criteria::addTitle(cli::Criterion _title_criteria) {}

std::vector<cli::Criterion> cli::Criteria::getGenres()
{
  return std::vector<cli::Criterion>();
}

void cli::Criteria::setGenres(std::vector<cli::Criterion> _new_genres)
{
  genres = _new_genres;
}

void cli::Criteria::addGenre(std::string _genre, uint16_t _quantity)
{
  genres.push_back(cli::Criterion(_genre, _quantity));
}

void cli::Criteria::addGenre(cli::Criterion _genre_criteria) {}

std::vector<cli::Criterion> cli::Criteria::getSubGenres()
{
  return std::vector<cli::Criterion>();
}

void cli::Criteria::setSubGenres(std::vector<cli::Criterion> _new_subgenres)
{
  subgenres = _new_subgenres;
}

void cli::Criteria::addSubGenre(std::string _subgenre, uint16_t _quantity)
{
  subgenres.push_back(cli::Criterion(_subgenre, _quantity));
}

void cli::Criteria::addSubGenre(cli::Criterion _subgenre_criteria) {}

std::vector<cli::Criterion> cli::Criteria::getArtists()
{
  return std::vector<cli::Criterion>();
}

void cli::Criteria::setArtists(std::vector<cli::Criterion> _new_artists)
{
  artists = _new_artists;
}

void cli::Criteria::addArtist(std::string _artist, uint16_t _quantity)
{
  artists.push_back(cli::Criterion(_artist, _quantity));
}

void cli::Criteria::addArtist(cli::Criterion _artist_criteria) {}

std::vector<cli::Criterion> cli::Criteria::getAlbums()
{
  return std::vector<cli::Criterion>();
}

void cli::Criteria::setAlbums(std::vector<cli::Criterion> _new_albums)
{
  albums = _new_albums;
}

void cli::Criteria::addAlbum(std::string _album, uint16_t _quantity)
{
  albums.push_back(cli::Criterion(_album, _quantity));
}

void cli::Criteria::addAlbum(cli::Criterion _album_criteria) {}
