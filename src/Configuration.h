/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_CONFIGURATION_H_
#define GENPL_SRC_CONFIGURATION_H_

#include <string>
#include <cstdint>

namespace db {

/** \class Configuration
 *
 * \brief Database configuration encapsulation
 *
 * The goal is to have all the PostgreSQL remote connection
 * configuration stored in one place
 */
class Configuration
{
 private:
  std::string user;  ///< The username that can access the #database
  std::string database;  ///< The database to which the #user should connect to
  std::string schema;  ///< The schema inside the #database that contains tables (default to public)
  std::string host;  ///< Host to connect to (FQDN or IPv4/v6 as string)
  uint16_t portTCP;  ///< The TCP port to connect to (default to 5432)

 public:
  /** \brief Default constructor
   */
  explicit Configuration();

  /** \brief Detailed constructor
   *
   * Construct an object with database parameters
   *
   * \param[in] _user The database user name
   *
   * \param[in] _database The database name
   *
   * \param[in] _schema The database schema where tables are stored
   *
   * \param[in] _host The database host server, IPv4, IPv6 or FQDN,
   * all as \c std::string
   *
   * \param[in] _portTCP The database host TCP port (default to 5432)
   */
  explicit Configuration(std::string _user,
                         std::string _database,
                         std::string _schema,
                         std::string _host,
                         uint16_t _portTCP = 5432);

  /** \brief Load database configuration from Json file
   *
   * The following elements are definable inside the \c db Json
   * object: \c user, \c database, \c schema, \c host, \c portTCP
   *
   * \param[in] _config_file Filename and path to the Json formatted
   * configuration file
   *
   * \par Example of configuration file in Json format
   * \include db.json
   */
  void load(std::string _config_file);

  /** \brief Print out summary
   */
  void summary() const;

  /** \brief Get the user name
   *
   * \return The user name stored in #user
   */
  std::string getUser();

  /** \brief Set the #user member
   *
   * \param _user The database user name
   */
  void setUser(std::string _user);

  /** \brief Get the database name
   *
   * \return The database name stored in #database
   */
  std::string getDatabase();

  /** \brief Set the #database member
   *
   * \param _database The database name
   */
  void setDatabase(std::string _database);

  /** \brief Get the schema name
   *
   * \return The schema name stored in #schema
   */
  std::string getSchema();

  /** \brief Set the #schema member
   *
   * \param _schema The schema name
   */
  void setSchema(std::string _schema);

  /** \brief Get the host name
   *
   * \return The host name stored in #host
   */
  std::string getHost();

  /** \brief Set the #host member
   *
   * \param _host The host name
   */
  void setHost(std::string _host);

  /** \brief Get the TCP port number
   *
   * \return The TCP port number stored in #portTCP
   */
  uint16_t getPortTCP();

  /** \brief Set the #portTCP member
   *
   * \param _portTCP The TCP port number
   */
  void setPortTCP(uint16_t _portTCP);
};
}

#endif  // GENPL_SRC_CONFIGURATION_H_
