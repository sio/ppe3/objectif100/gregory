/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#include "Analyzer.h"

#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

#ifdef HAVE_CONFIG_H
  #include "config.h"
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(PACKAGE".cli"));
#else
  static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("cli"));
#endif

cli::Analyzer *cli::Analyzer::singleton = nullptr;

cli::Analyzer *cli::Analyzer::instance()
{
  if(!singleton)
  {
    singleton = new Analyzer();
  }

  return singleton;
}

cli::Analyzer::Analyzer() :
  argc(0),
  argv(nullptr),
  duration(0),
  name(),
  path(),
  description(),
  loglevel(log4cxx::Level::getError()->toString()),
  M3U_output(true),
  XSPF_output(false),
  criteria(),
  db_config_file(),
  db_configuration()
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
}

void cli::Analyzer::parse(int _argc, char **_argv)
{
  argc = _argc;
  argv = _argv;

  try
  {
    TCLAP::CmdLine cmd("Automated playlist generator, based on criteria and with M3U and XSPF output format",
                       ' ',
#ifdef PACKAGE_VERSION
                       PACKAGE_VERSION);
#else
                       "local-version");
#endif
    // Global options
    TCLAP::UnlabeledValueArg<uint16_t> duration_UnlblArg("duration",
        "Playlist duration in minutes",
        true, 120, "TIME", cmd);
    TCLAP::ValueArg<std::string> name_Arg("", "name",
                                          "Playlist name",
                                          false, std::string("playlist"), "TEXT", cmd);
    TCLAP::ValueArg<std::string> path_Arg("", "directory",
                                          "Base output directory path",
                                          false, std::string("."), "PATH", cmd);
    TCLAP::ValueArg<std::string> description_Arg("", "description",
        "Playlist name",
        false, std::string("playlist"), "TEXT", cmd);
    std::vector<std::string> loglevels;
    loglevels.push_back(log4cxx::Level::getDebug()->toString());
    loglevels.push_back(log4cxx::Level::getInfo()->toString());
    loglevels.push_back(log4cxx::Level::getError()->toString());
    loglevels.push_back(log4cxx::Level::getFatal()->toString());
    TCLAP::ValuesConstraint<std::string> allowed_loglevels(loglevels);
    TCLAP::ValueArg<std::string> loglevel_Arg(
      "", "loglevel",
      "Loglevel, default to " + log4cxx::Level::getError()->toString(),
      false, log4cxx::Level::getError()->toString(), &allowed_loglevels, cmd);
    TCLAP::SwitchArg M3U_output_SwArg(
      "", "M3U",
      "Output to M3U (default, even if not specified)",
      cmd, true);
    TCLAP::SwitchArg XSPF_output_SwArg(
      "", "XSPF",
      "Output to XSPF",
      cmd, false);
    // Criteria options
    TCLAP::MultiArg<cli::Criterion> title_Arg(
      "t", "title",
      "Title pattern and quantity",
      false, "PATTERN=QUANTITY", cmd);
    TCLAP::MultiArg<cli::Criterion> genre_Arg(
      "g", "genre",
      "Genre pattern and quantity",
      false, "PATTERN=QUANTITY", cmd);
    TCLAP::MultiArg<cli::Criterion> subgenre_Arg(
      "s", "subgenre",
      "Subgenre pattern and quantity",
      false, "PATTERN=QUANTITY", cmd);
    TCLAP::MultiArg<cli::Criterion> artist_Arg(
      "a", "artist",
      "Artist pattern and quantity",
      false, "PATTERN=QUANTITY", cmd);
    TCLAP::MultiArg<cli::Criterion> album_Arg(
      "A", "album",
      "Album pattern and quantity",
      false, "PATTERN=QUANTITY", cmd);
    // Databases options
    TCLAP::ValueArg<uint16_t> db_portTCP_Arg(
      "", "db-port-tcp",
      "PostgreSQL database TCP host port (override configuration file)",
      false, 5432, "INTEGER", cmd);
    TCLAP::ValueArg<std::string> db_host_Arg(
      "", "db-host",
      "PostgreSQL database FQDN hostname (override configuration file)",
      false, "", "TEXT", cmd);
    TCLAP::ValueArg<std::string> db_schema_Arg(
      "", "db-schema",
      "PostgreSQL database schema to work within (override configuration file)",
      false, "", "TEXT", cmd);
    TCLAP::ValueArg<std::string> db_database_Arg(
      "", "db-database",
      "PostgreSQL database name to connect to (override configuration file)",
      false, "", "TEXT", cmd);
    TCLAP::ValueArg<std::string> db_user_Arg(
      "", "db-user",
      "PostgreSQL database user login to connect with (override configuration file)",
      false, "", "TEXT", cmd);
    TCLAP::ValueArg<std::string> config_file_Arg(
      "", "db-config",
      "PostgreSQL database configuration file path",
      false, "", "FILENAME", cmd);
    // Parse the args.
    cmd.parse(argc, argv);
    // Get the value parsed by each arg.
    duration = duration_UnlblArg.getValue();
    name = name_Arg.getValue();
    path = path_Arg.getValue();
    description = description_Arg.getValue();
    loglevel = loglevel_Arg.getValue();
    logger->setLevel(log4cxx::Level::toLevel(loglevel));
    M3U_output = M3U_output_SwArg.getValue();
    XSPF_output = XSPF_output_SwArg.getValue();
    criteria.setTitles(title_Arg.getValue());
    criteria.setGenres(genre_Arg.getValue());
    criteria.setSubGenres(subgenre_Arg.getValue());
    criteria.setArtists(artist_Arg.getValue());
    criteria.setAlbums(album_Arg.getValue());

    if(config_file_Arg.isSet())
    {
      db_configuration.load(config_file_Arg.getValue());
    }

    if(db_user_Arg.isSet())
    {
      db_configuration.setUser(db_user_Arg.getValue());
    }

    if(db_database_Arg.isSet())
    {
      db_configuration.setDatabase(db_database_Arg.getValue());
    }

    if(db_schema_Arg.isSet())
    {
      db_configuration.setSchema(db_schema_Arg.getValue());
    }

    if(db_host_Arg.isSet())
    {
      db_configuration.setHost(db_host_Arg.getValue());
    }

    if(db_portTCP_Arg.isSet())
    {
      db_configuration.setPortTCP(db_portTCP_Arg.getValue());
    }
  }
  catch(TCLAP::ArgException &e)     // catch any exceptions
  {
    std::ostringstream output;
    output << e.error() << " for " << e.argId();
    logger->error(output.str());
  }
}

void cli::Analyzer::summary() const
{
  std::ostringstream output;
  output << std::endl << std::boolalpha
         << "[Arguments and flags summary]" << std::endl
         << "  Duration: " << duration << std::endl
         << "  Name: " << name << std::endl
         << "  Output directory: " << path << std::endl
         << "  Description: " << description << std::endl
         << "  Loglevel: " << loglevel << std::endl
         << "  M3U output: " << M3U_output << std::endl
         << "  XSPF output: " << XSPF_output << std::endl;
  logger->info(output.str());
  criteria.summary();
  db_configuration.summary();
}

const cli::Criteria *cli::Analyzer::getCriteria()
{
  return &criteria;
}

const db::Configuration *cli::Analyzer::getConfiguration()
{
  return &db_configuration;
}

std::string cli::Analyzer::getDB_config_file()
{
  return db_config_file;
}

uint16_t cli::Analyzer::getDuration()
{
  return duration;
}

std::string cli::Analyzer::getName()
{
  return name;
}

std::string cli::Analyzer::getPath()
{
  return path;
}

std::string cli::Analyzer::getLogLevel()
{
  return loglevel;
}

bool cli::Analyzer::getM3U_output()
{
  return M3U_output;
}

bool cli::Analyzer::getXSPF_output()
{
  return XSPF_output;
}

std::string cli::Analyzer::getDescription()
{
  return description;
}

int cli::Analyzer::getArgc()
{
  return argc;
}

char **cli::Analyzer::getArgv()
{
  return argv;
}
