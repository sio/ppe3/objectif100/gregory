/* This file is part of the 'genpl' program
 * (https://framagit.org/sio/ppe3/objectif100/gregory)
 *
 * Copyright (C) 2019 Grégory David <gregory.david@ac-nantes.fr>
 *
 *  This program is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see
 *  <https://www.gnu.org/licenses/\>.
*/

#ifndef GENPL_SRC_CRITERIA_H_
#define GENPL_SRC_CRITERIA_H_

#include <vector>
#include <string>
#include <ostream>

#include <tclap/CmdLine.h>

namespace cli {

/** \class Criterion
 *
 */
class Criterion
{
 public:
  std::string pattern;
  uint16_t quantity;

  Criterion();
  Criterion(std::string _pattern, uint16_t _quantity);
  ~Criterion();
  Criterion &operator=(const std::string &input_string);
  std::string summary() const;
};

/** \class Criteria
 *
 * \brief Encapsulate all available criteria
 *
 * The aim of the class is to encapsulate criteria that are offered to
 * user when calling the program. Each private member should be a
 * container for what is waited on command line.
 *
 */
class Criteria
{
 private:
  std::vector<cli::Criterion> titles;  ///< A \c std::vector container to store all titles given by user on CLI
  std::vector<cli::Criterion> genres;  ///< A \c std::vector container to store all genres given by user on CLI
  std::vector<cli::Criterion> subgenres;  ///< A \c std::vector container to store all subgenres given by user on CLI
  std::vector<cli::Criterion> artists;  ///< A \c std::vector container to store all artists given by user on CLI
  std::vector<cli::Criterion> albums;  ///< A \c std::vector container to store all albums given by user on CLI

 public:
  /** \brief Default constructor
   *
   * It has all criteria set to empty container.
   */
  explicit Criteria();

  /** \brief Print out the criteria summary
   */
  void summary() const;


  /** \brief Get the #titles container
   *
   * \return A copy of the #titles \c std::vector container
   */
  std::vector<cli::Criterion> getTitles();

  /** \brief Set the #titles container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_titles will
   * replace the #titles inplace.
   *
   * \param[in] _new_titles The replacing #titles container
   */
  void setTitles(std::vector<cli::Criterion> _new_titles);

  /** \brief Add a new title to the #titles container
   *
   * \param[in] _title The title name to be added to the #titles container
   * as key
   *
   * \param[in] _quantity The corresponding quantity to be added to the
   * #titles container as value
   */
  void addTitle(std::string _title, uint16_t _quantity);

  /** \brief Add a new title to the #titles container
   *
   * \param[in] _title_criteria The title string containing the
   * pattern and the quantity
   */
  void addTitle(cli::Criterion _title_criteria);


  /** \brief Get the #genres container
   *
   * \return A copy of the #genres \c std::vector container
   */
  std::vector<cli::Criterion> getGenres();

  /** \brief Set the #genres container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_genres will
   * replace the #genres inplace.
   *
   * \param[in] _new_genres The replacing #genres container
   */
  void setGenres(std::vector<cli::Criterion> _new_genres);

  /** \brief Add a new genre to the #genres container
   *
   * \param[in] _genre The genre name to be added to the #genres container
   * as key
   *
   * \param[in] _quantity The corresponding quantity to be added to the
   * #genres container as value
   */
  void addGenre(std::string _genre, uint16_t _quantity);

  /** \brief Add a new genre to the #genres container
   *
   * \param[in] _genre_criteria The genre string containing the
   * pattern and the quantity
   */
  void addGenre(cli::Criterion _genre_criteria);


  /** \brief Get the #subgenres container
   *
   * \return A copy of the #subgenres \c std::vector container
   */
  std::vector<cli::Criterion> getSubGenres();

  /** \brief Set the #subgenres container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_subgenres
   * will replace the #subgenres inplace.
   *
   * \param[in] _new_subgenres The replacing #subgenres container
   */
  void setSubGenres(std::vector<cli::Criterion> _new_subgenres);

  /** \brief Add a new subgenre to the #subgenres container
   *
   * \param[in] _subgenre The subgenre name to be added to the
   * #subgenres container as key
   *
   * \param[in] _quantity The corresponding quantity to be added to
   * the #subgenres container as value
   */
  void addSubGenre(std::string _subgenre, uint16_t _quantity);

  /** \brief Add a new SubGenre to the #subgenres container
   *
   * \param[in] _subgenre_criteria The SubGenre string containing the
   * pattern and the quantity
   */
  void addSubGenre(cli::Criterion _subgenre_criteria);


  /** \brief Get the #artists container
   *
   * \return A copy of the #artists \c std::vector container
   */
  std::vector<cli::Criterion> getArtists();

  /** \brief Set the #artists container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_artists will
   * replace the #artists inplace.
   *
   * \param[in] _new_artists The replacing #artists container
   */
  void setArtists(std::vector<cli::Criterion> _new_artists);

  /** \brief Add a new artist to the #artists container
   *
   * \param[in] _artist The artist name to be added to the #artists
   * container as key
   *
   * \param[in] _quantity The corresponding quantity to be added to
   * the #artists container as value
   */
  void addArtist(std::string _artist, uint16_t _quantity);

  /** \brief Add a new artist to the #artists container
   *
   * \param[in] _artist_criteria The artist string containing the
   * pattern and the quantity
   */
  void addArtist(cli::Criterion _artist_criteria);


  /** \brief Get the #albums container
   *
   * \return A copy of the #albums \c std::vector container
   */
  std::vector<cli::Criterion> getAlbums();

  /** \brief Set the #albums container by replacing the existing one
   *
   * The \c std::vector container given as parameter \c _new_albums will
   * replace the #albums inplace.
   *
   * \param[in] _new_albums The replacing #albums container
   */
  void setAlbums(std::vector<cli::Criterion> _new_albums);

  /** \brief Add a new album to the #albums container
   *
   * \param[in] _album The album name to be added to the #albums
   * container as key
   *
   * \param[in] _quantity The corresponding quantity to be added to
   * the #albums container as value
   */
  void addAlbum(std::string _album, uint16_t _quantity);

  /** \brief Add a new album to the #albums container
   *
   * \param[in] _album_criteria The album string containing the
   * pattern and the quantity
   */
  void addAlbum(cli::Criterion _album_criteria);
};
}

#endif  // GENPL_SRC_CRITERIA_H_
